module traffic 
( 
 input wire clk ,
 input op ,
 output wire [6:0] a1 ,a2 , a3 , b1 , b2 , b3 , c1 , c2 , c3 , d1 , d2 ,d3 ,
 output wire [1:0] e1 ,e2 , e3, e4 
 ); 

//in begin all counter must load data variable so all of them are 7
reg[2:0] state ,load= 3'b111, load1= 3'b111 , load2 =3'b111 , load3 = 3'b111 , load4 =3'b111;

//in begin just first light is green(1) and other are red(3)
reg [1:0] load5= 2'b01 , load6= 2'b11 , load7=2'b11 ,load8=2'b11;

wire[7:0] z1, z2, z3, z4, b ;

reg [6:0] tmp1 = 50 , tmp2 = 85;

parameter 
 S0 = 0 ,
 S1 = 1 ,
 S2 = 2 ,// states      
 S3 = 3 ,
 S4 = 4 ,
 S5 = 5 ,
 S6 = 6 ,
 S7 = 7 ;
 
 //for change data of main counter 
 reg [7:0] temp_main;
 
 initial
	temp_main = 0;

 counter 		main_c 			(.out(b)	 , .data(temp_main) 	, .load(load) 		, .up_down(1) , .clk(clk) , .reset(0));
 counter 		first_c 			(.out(z1) , .data(45) 			, .load(load1) 	, .up_down(0) , .clk(clk) , .reset(0));
 counter 		second_c			(.out(z2) , .data(tmp1) 		, .load(load2) 	, .up_down(0) , .clk(clk) , .reset(0));
 counter 		third_c 			(.out(z3) , .data(tmp2) 		, .load(load3) 	, .up_down(0) , .clk(clk) , .reset(0));
 counter 		fourth_c 		(.out(z4) , .data(135) 			, .load(load4) 	, .up_down(0) , .clk(clk) , .reset(0));
 
 display_led 	first_led		(.a(z1), .x(a1), .y(a2), .z(a3));
 display_led 	second_led		(.a(z2), .x(b1), .y(b2), .z(b3));
 display_led 	third_led		(.a(z3), .x(c1), .y(c2), .z(c3));
 display_led 	fourth_led		(.a(z4), .x(d1), .y(d2), .z(d3));
 
 color 			first_color 	(.ccolor(e1) , .load(load5) , .clk(clk));
 color 			second_color 	(.ccolor(e2) , .load(load6) , .clk(clk));
 color 			third_color 	(.ccolor(e3) , .load(load7) , .clk(clk));
 color 			fourth_color	(.ccolor(e4) , .load(load8) , .clk(clk));

always @(posedge clk)  
 begin  
 case(state) 
 S0: if(b <= 45 && !op)
 begin
 state <= S0; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end 
 else  
 begin
 temp_main <= 45;
 load <= 7;
 state <= S1;
 load1 <= 2;
 load5 <= 2;
 end   
 S1: if(b <= 50 && !op)  
 begin         
 state <= S1;   
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end
 else      
 begin
 //for when op pressed
 temp_main <= 50;
 load <= 7;
 
 state <= S2;  
 load1 <= 5;
 load2 <= 3;
 load5 <= 3;
 load6 <= 1;
 end  
 S2: if(b <= 80 && !op)  
 begin       
 state <= S2; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ; 
 end  
 else  
 begin
 temp_main <= 80;
 load <= 7;
 state <= S3; 
 load2 <= 2;
 load6 <= 2;
 end      
 S3: if(b <= 85 && !op)     
 begin        
 state <= S3; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0;
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ; 
 end   
 else     
 begin
 temp_main <= 85;
 load <= 7;
 state <= S4;      
 load2 <= 6;
 load3 <= 4;
 load6 <= 3;
 load7 <= 1;
 end  
 S4: if(b <= 130 && !op)  
 begin        
 state <= S4;
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end   
 else  
 begin
 temp_main <= 130;
 load <= 7;
 state <= S5;
 load3 <= 2; 
 load7 <= 2;
 end  
 S5: if(b <= 135 && !op)    
 begin  
 state <= S5; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end  
 else 
 begin
 temp_main <= 135;
 load <= 7;
 state <= S6;
 load3 <= 5;
 load4 <= 3;
 load7 <= 3;
 load8 <= 1;
 end
  S6: if(b <= 165 && !op)    
 begin  
 state <= S6; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end  
 else 
 begin
 temp_main <= 165;
 load <= 7;
 state <= S7;
 load4 <= 2 ;
 load8 <= 2;
 end
  S7: if(b <= 170 && !op)    
 begin  
 state <= S7; 
 load <= 0;
 load1 <= 0;
 load2 <= 0;
 load3 <= 0;
 load4 <= 0; 
 load5 <= 0;
 load6 <= 0 ; 
 load7 <= 0 ;
 load8 <= 0 ;
 end  
 else 
 begin
 temp_main <= 170;
 load <= 7;
 state <= S0;
 load <= 1;
 load1 <= 4;
 load2 <= 7;
 load3<= 7;
 load4 <= 6;
 load5 <= 1;
 load6 <= 3;
 load7 <= 3;
 load8 <= 3;
 end
 default state <= S0;
 endcase
 end   

 endmodule         
 