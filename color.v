module color
(
 ccolor,
 load,
 clk
);

output reg [1:0] ccolor;
input clk ;
input [1:0] load;

always @(posedge clk)

	if (load == 2'b00)				//if load == 0, load prev color
		ccolor <= ccolor;
	 
	else if ( load == 2'b01)		//if load == 1, load 1(green)
		ccolor <= 1;
	
	else if ( load == 2'b10)		//if load == 2, load 2(yellow)
		ccolor <= 2;
	
	else 
		ccolor <= 3;					//if load == 3, load 3(red)
	
endmodule
