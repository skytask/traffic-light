module display_led(a, x, y, z);
input [11:0] a;
output [6:0] x, y, z;

wire [3:0] yek, dah, sad;

//for disable in seven segment split yekan and dahgan and sadgan
assign yek = a % 10;
assign dah = (a / 10) % 10;
assign sad = (a / 100);

seven_seg ins_1(.a(sad)		, .z(x));
seven_seg ins_2(.a(dah)		, .z(y));
seven_seg ins_3(.a(yek)		, .z(z));

endmodule
