onerror {exit -code 1}
vlib work
vlog -work work single.vo
vlog -work work Waveformtraffic.vwf.vt
vsim -novopt -c -t 1ps -L maxii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate_ver -L altera_lnsim_ver work.traffic_vlg_vec_tst -voptargs="+acc"
vcd file -direction single.msim.vcd
vcd add -internal traffic_vlg_vec_tst/*
vcd add -internal traffic_vlg_vec_tst/i1/*
run -all
quit -f
