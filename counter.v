module counter  (
out      ,  // Output of the counter
data     ,  // Parallel load for the counter
load     ,  // Parallel load enable
up_down   ,  //up or down
clk      ,  // clock input
reset       // reset input
);


input [7:0] data;
input  clk, reset;
input up_down;
input [2:0] load;

output reg  [7:0] out;

initial
	out = 0;

always @(posedge clk)
	if (reset)
		out <= 0 ;
		
	else if (load == 3'b111)		//if load == 7, load (data input) in counter
	 out <= data;
	 
	else if (load == 3'b001) 		//if load == 1, load 0 in counter
		 out <= 0;
	
	else if (load == 3'b010) 		//if load == 2, load 5 in counter
		 out <= 8'b00000101;
	
	else if (load == 3'b011) 		//if load == 3, load 30 in counter
		 out <= 30;
	
	else if (load == 3'b100) 		//if load == 4, load 45 in counter
		 out <= 45;
	
	else if (load == 3'b101) 		//if load == 5, load 120 in counter
		 out <= 120;
	
	else if (load == 3'b110) 		//if load == 6, load 135 in counter
		 out <= 135;
	
	else 
		 out <= out + (up_down ? 1 : -1);
		 
endmodule
